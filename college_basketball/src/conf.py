#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
configs file
@author: Matt Tetkoskie - 01.17.2022
"""
import os

BASE_PATH = os.getcwd().split('college_basketball')[0] + 'college_basketball/'
DATA_DIR = BASE_PATH + '/data/'
MODEL_DIR = BASE_PATH + '/models/'
modelVersion = 2.021022
strategyVersion = '2.021022s'

spreadEVcutoff = 0.01
mlEVcutoff = 0.02

betColumns = ['Team','Spread','SpreadOdds', 'Total', 'TotalOdds','MONEYLINE','team_name',
              'gameID', 'finalPred', 'projectedSpread', 'spreadDiff_abs', 'projSpreadWin', 'projGameWin',
              'probability_to_win', 'spread_expected_value', 'moneyline_expected_value', 'betIndicator_spread', 'betIndicator_ML']

mlBetCols = ['Team', 'MONEYLINE', 'Spread', 'projectedSpread', 'probability_to_win', 'moneyline_expected_value']
spreadBetCols = ['Team', 'Spread', 'SpreadOdds', 'projectedSpread', 'probability_to_win', 'spread_expected_value']