#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

This file is the main data pipeline for NCAA BB predictions

@author: Matt Tetkoskie - 01.17.2022

"""

import os, sys
from WebScraper import WebScraper
scraper = WebScraper()
import conf
from datetime import datetime, timedelta

base_path = os.getcwd().split('college_basketball')[0] + 'college_basketball/'
dataDir = base_path + '/data/'
print('BasePath: %s' %base_path)

modelDir = base_path + '/models/'
sys.path.append(base_path+'/src')

from gameDay import GameDay

class DataPipeline:

    def __init__(self, date):
        self.date = date
        self.yesterday = datetime.strptime(self.date, '%m-%d-%Y') - timedelta(days=1)
        self.yesterdayString = self.yesterday.strftime('%m-%d-%Y')

    def get_results(self):
        """
        Get results from yesterday and write to CSV
        :return: pandas dataframe
        """
        # Get results from yesterday
        scraper.scrape_results(self.yesterday.strftime('%m-%d-%Y'))

    def run(self):
        """
        This method identifies best bets for a given date
        :return:
        """
        # Scrape Data for Today
        gameDay = GameDay(self.date)
        # Generate Predictions
        bets = gameDay.get_predictions()
        bets.to_csv(dataDir + 'predictions/scorePredictions_%s.csv' % self.date, index=False)
        bets.to_csv(dataDir + 'betAdvice/%s_betAdvice.csv' % self.date, index=False)

        # Identify Bets
        mlBets = bets[bets['betIndicator_ML']].copy()
        print('Identified %d moneyline bets today' %len(mlBets))
        print(mlBets[conf.mlBetCols])

        print("*******")

        spreadBets = bets[bets['betIndicator_spread']].copy()
        print('Identified %d spread bets today' % len(spreadBets))
        print(spreadBets[conf.spreadBetCols])
        #Generate stats from yesterday

        # Send email
        return bets

if __name__ == '__main__':
    today = datetime.today().strftime('%m-%d-%Y')
    a = DataPipeline(today)
    a.get_results()
    a.run()
