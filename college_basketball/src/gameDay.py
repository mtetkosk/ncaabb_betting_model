#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

This contains code for the game day class

@author: Matt Tetkoskie - 01.17.2022

"""

import utils
import conf
import pandas as pd
from datetime import datetime
# Initiate web scraper
from WebScraper import WebScraper
scraper = WebScraper()

# Initiate model file
from model import Model
model = Model()

# Initiate strategy module
from strategy import BettingStrategy

class GameDay:

    def __init__(self, date, writeData=True):
        self.writeData = writeData
        self.date = date
        self.games = scraper.scrape_games_today(writeData=writeData)
        scraper.scrape_kenpom(writeData=True)
        self.kenPom = pd.read_csv(conf.DATA_DIR + 'kenPomDaily/kp_%s.csv' % date)
        self.teamRankings = scraper.scrape_teamRankings(datetime.strptime(date, '%m-%d-%Y').strftime('%Y-%m-%d'),
                                                        writeData=writeData)
        # Get Odds
        self.odds = scraper.scrape_odds(writeData=writeData)
        #self.odds = pd.read_csv(conf.DATA_DIR+'OddsDaily/odds_%s.csv' % date)

        # Merge data from multiple data sources
        self.nGames = len(self.games)
        self.master = self.merge_data()

    def merge_data(self):
        # Normalize team names from multiple data sources
        self.teamRankings['Team'] = utils.formatTeamName(self.teamRankings['Team'])
        self.kenPom['Team'] = utils.formatTeamName(self.kenPom['Team'])
        self.odds['Team'] = utils.formatTeamName(self.odds['Team'])
        self.games['away_name'] = utils.formatTeamName(self.games['away_name'])
        self.games['home_name'] = utils.formatTeamName(self.games['home_name'])

        # Perform Joins
        # Join Stats on TeamRankings Data
        statsRankings = pd.merge(self.kenPom, self.teamRankings, on='Team')
        # Join master to games data - away_team
        master = pd.merge(self.games, statsRankings, left_on='away_name', right_on='Team')
        # Join master to games data - home_team
        master = pd.merge(master, statsRankings, left_on='home_name', right_on='Team')
        master.columns = utils.formatColumnNames(list(master.columns))
        # Create GameID Column
        master['date'] = self.date
        master['gameID'] = master['date'] + '-' + master['away_abbr'] + '-' + master['home_abbr']
        master = master.reset_index(drop=True)
        return master

    def get_predictions(self):
        """ This method invokes the Model class to generate features and get predictions """
        features = model.create_features(self.master)
        # Set features as attributes
        #self.features = features
        predictions = model.predict(features)
        # Set predictions as an attribute
        self.predictions = predictions
        # Join Features + Predictions + OddsData
        predictionDataMaster = pd.merge(model.mlFeatures, self.odds, left_on='team_name', right_on='Team')
        # Sort By GameID
        predictionDataMaster = predictionDataMaster.sort_values('gameID').reset_index(drop=True)
        # Call Strategy Module
        strategyModel = BettingStrategy(predictionDataMaster)
        bets = strategyModel.get_bets()
        # Reduce Columns
        return bets[conf.betColumns]

if __name__ == '__main__':
    a = GameDay(datetime.today().strftime('%m-%d-%Y'))
    df = a.get_predictions()
    #df = a.get_prediction_data()
    #print(df.head())
    df.to_csv('BetsTest.csv', index=False)
