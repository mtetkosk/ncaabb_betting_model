#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

This contains code for feature building

@author: Matt Tetkoskie - 12.08.2021

"""

import os
import utils
import pandas as pd
from ClusterTeams import ClusterFeatures
clusterEngine = ClusterFeatures()

base_path = os.getcwd().split('college_basketball')[0]  + 'college_basketball/'
dataDir = base_path + '/data/'


class CreateFeatures():

    def build_features_team_total(self, inputDf = None):
        """
        This method calculates features for team data. Predicting point totals

        :return: pandas dataframe
        """

        if inputDf is None:
            ## Load Game Data Files
            master = pd.DataFrame()
            for filename in os.listdir(dataDir+'processed/teamData/'):
                if '.csv' not in filename:
                    continue
                print('Loading game data: %s' %filename)
                df = pd.read_csv(dataDir+'processed/teamData/'+filename)
                master = pd.concat([master, df])

            numSamples = len(master)
            print('Loaded %d samples' %numSamples)
            print(master.shape)

        else:
            master = inputDf.copy()

        # Create home away
        print('got master of length %d' % len(master))
        master['home_away'] = [1 if x == 'home' else -1 for x in master['homeAway']]
        homeBinary = [1 if x == 1 else 0 for x in master['homeAway']]
        awayBinary = [1 if x == -1 else 0 for x in master['homeAway']]

        # Create features which capture the differences between team and opponent
        master['kp_rank_diff'] = master['Rk'] - master['Rk_opposition']
        # Binned Rank Diff Variables
        master['kp_rank_diff_large_favorite'] = [1 if x <= -200 else 0 for x in master['kp_rank_diff']]
        master['large_favorite_home'] = master['kp_rank_diff_large_favorite']*homeBinary
        master['kp_rank_diff_medium_favorite'] = [1 if -200 < x <= -100 else 0 for x in master['kp_rank_diff']]
        master['medium_favorite_home'] = master['kp_rank_diff_medium_favorite']*homeBinary
        master['kp_rank_diff_slight_favorite'] = [1 if -100 < x <= 0 else 0 for x in master['kp_rank_diff']]
        master['kp_rank_diff_slight_underdog'] = [1 if 0 < x <= 100 else 0 for x in master['kp_rank_diff']]
        master['kp_rank_diff_medium_underdog'] = [1 if 100 < x <= 200 else 0 for x in master['kp_rank_diff']]
        master['medium_dog_home'] = master['kp_rank_diff_medium_underdog']*homeBinary
        master['kp_rank_diff_large_underdog'] = [1 if 200 < x <= 300 else 0 for x in master['kp_rank_diff']]
        master['large_dog_home'] = master['kp_rank_diff_large_underdog']*homeBinary

        # Make features around absolute rank - bins
        master['eliteTeam'] = [1 if x <= 10 else 0 for x in master['Rk']]
        master['eliteTeam_opposition'] = [1 if x <= 10 else 0 for x in master['Rk_opposition']]
        master['eliteMatchup'] = master['eliteTeam'] * master['eliteTeam_opposition']
        master['top25'] = [1 if 10 < x <= 25 else 0 for x in master['Rk']]
        master['top25_opposition'] = [1 if 10 < x <= 25 else 0 for x in master['Rk_opposition']]
        master['bottomFeeder'] = [1 if x > 300 else 0 for x in master['Rk']]
        master['bottomFeeder_opposition'] = [1 if x > 300 else 0 for x in master['Rk_opposition']]
        master['EliteVsBottom'] = master['eliteTeam'] * master['bottomFeeder_opposition']


        diffCols = ['AdjEM', 'AdjO', 'AdjO_rank', 'AdjD', 'AdjD_rank', 'AdjT', 'AdjT_rank', 'Luck', 'Luck_rank',
                    'AdjEM_rank',
                    'AdjEM_rank_rank', 'OppO', 'OppO_rank', 'OppD', 'OppD_rank', 'AdjEM.2', 'AdjEM_rank.2']

        for col in diffCols:
            master[col + '_diff'] = master[col] - master[col + '_opposition']

        # Create conference binary flags
        confDummies= pd.get_dummies(master['Conf'], prefix='conference')
        confDummies_opposition= pd.get_dummies(master['Conf_opposition'], prefix='conference_opposition')

        master = pd.concat([master, confDummies], axis=1)
        master = pd.concat([master, confDummies_opposition], axis=1)

        # Identify conference games
        sameConf = master['Conf'] == master['Conf_opposition']
        master['conference_matchup'] = [1 if True else 0 for x in sameConf]
        Power5 = ['B12', 'B10', 'ACC', 'P12', 'SEC']
        master['power5Conf'] = [1 if x in Power5 else 0 for x in master['Conf']]
        master['power5Opp'] = [1 if x in Power5 else 0 for x in master['Conf_opposition']]
        p5vsNonp5 = master['power5Conf'] - master['power5Opp']
        master['p5vsNonP5'] = p5vsNonp5

        # Create features around ppg and 3's per game
        ppgLast3Diff = master['Last3_ppg'] - master['2021_ppg']
        scoringHot = [1 if x > 5 else 0 for x in ppgLast3Diff]
        master['scoring_hot'] = scoringHot
        scoringCold = [1 if x < -5 else 0 for x in ppgLast3Diff]
        master['scoring_cold'] = scoringCold

        # Reset Index
        master = master.reset_index(drop=True)
        # Home/Away PPG
        ppg = []
        ppg_opp = []
        threePT = []
        threePTopp = []
        for ix in range(len(master)):
            homeAway = master['home_away'][ix]
            if homeAway == 1:
                ppg.append(master['Home_ppg'][ix])
                threePT.append(master['Home_3ptpct'][ix])
                ppg_opp.append(master['Away_ppg'][ix])
                threePTopp.append(master['Away_3ptpct'][ix])
            else:
                ppg.append(master['Away_ppg'][ix])
                threePT.append(master['Away_3ptpct'][ix])
                ppg_opp.append(master['Home_ppg'][ix])
                threePTopp.append(master['Home_3ptpct'][ix])

        master['ppg_homeaway'] = utils.clean_tr_data_col('--', ppg)
        master['ppg_homeway_opp'] = utils.clean_tr_data_col('--', ppg_opp)
        master['threePT_homeaway'] = utils.clean_tr_data_col('--', threePT)
        master['threePT_homeaway_opp'] = utils.clean_tr_data_col('--', threePTopp)

        removeCols = ['Home_ppg', 'Home_ppg_opposition', 'Away_ppg', 'Away_ppg_opposition', 'Home_3ptpct',
                      'Home_3ptpct_opposition', 'Away_3ptpct', 'Away_3ptpct_opposition']

        for col in removeCols:
            if col in list(master.columns):
                del master[col]

        # Tempo Categorical Vars
        master['tempoFast'] = [1 if x >= 72 else 0 for x in master['AdjT']]
        master['tempoSlow'] = [1 if x <= 64 else 0 for x in master['AdjT']]
        master['tempoMismatch'] = [1 if abs(x)>5 else 0 for x in master['AdjT_diff']]
        # Define the tempo for the highest ranked team
        highestRankTempo = []
        for ix in range(len(master)):
            rkDiff = master['kp_rank_diff'][ix]
            if rkDiff < 0:
                highestRankTempo.append(master['AdjT'][ix])
            else:
                highestRankTempo.append(master['AdjT_opposition'][ix])
        master['highestRankTempo'] = highestRankTempo


        # Generate expected points based on kp rank diff
        m = -0.038
        b = 70.03
        homeOffset = 2.25
        expectedPoints = (m*master['kp_rank_diff']+b)+homeOffset*master['home_away']
        master['kp_rank_diff_linear_expected_points'] = expectedPoints
        print('generated points diff no prob')
        # Create cluster features
        teamCluster = clusterEngine.clusterTeam(master[['AdjEM', 'AdjO', 'AdjD', 'AdjT']].copy())
        master['teamCluster'] = teamCluster
        clusterDummies = pd.get_dummies(master['teamCluster'])
        clusterDummies.columns = ['teamCluster_' + str(x) for x in clusterDummies.columns]
        # Check for all cluster features
        # If there are no teams in a cluster, it can throw an error
        for expectedClusterVal in [0, 1, 2, 3, 4, 5]:
            if 'teamCluster_%d' %expectedClusterVal not in list(clusterDummies.columns):
                print('Adding cluster col: oppCluster %d' % expectedClusterVal)
                clusterDummies['teamCluster_%d' % expectedClusterVal] = 0
        master = pd.concat([master, clusterDummies], axis=1)
        oppCluster = clusterEngine.clusterTeam(master[['AdjEM_opposition', 'AdjO_opposition', 'AdjD_opposition', 'AdjT_opposition']].copy())
        master['oppCluster'] = oppCluster
        clusterDummies = pd.get_dummies(master['oppCluster'])
        clusterDummies.columns = ['oppCluster_' + str(x) for x in clusterDummies.columns]
        for expectedClusterVal in [0, 1, 2, 3, 4, 5]:
            if 'oppCluster_%d' %expectedClusterVal not in list(clusterDummies.columns):
                print('Adding cluster col: oppCluster %d' % expectedClusterVal)
                clusterDummies['oppCluster_%d' % expectedClusterVal] = 0
        master = pd.concat([master, clusterDummies], axis=1)

        master['cluster0v1Home'] = master['teamCluster_0'] * master['oppCluster_1'] * homeBinary
        master['cluster0v1Away'] = master['teamCluster_0'] * master['oppCluster_1'] * awayBinary

        master['cluster1v2Home'] = master['teamCluster_1'] * master['oppCluster_2'] * homeBinary
        master['cluster1v2Away'] = master['teamCluster_1'] * master['oppCluster_2'] * awayBinary

        master['cluster1v4Home'] = master['teamCluster_1'] * master['oppCluster_4'] * homeBinary
        master['cluster1v4Away'] = master['teamCluster_1'] * master['oppCluster_4'] * awayBinary

        master['cluster1v5Home'] = master['teamCluster_1'] * master['oppCluster_5'] * homeBinary
        master['cluster1v5Away'] = master['teamCluster_1'] * master['oppCluster_5'] * awayBinary

        master['cluster1v0Home'] = master['teamCluster_1'] * master['oppCluster_0'] * homeBinary
        master['cluster1v0Away'] = master['teamCluster_1'] * master['oppCluster_0'] * awayBinary

        master['cluster2v0Home'] = master['teamCluster_2'] * master['oppCluster_0'] * homeBinary
        master['cluster2v0Away'] = master['teamCluster_2'] * master['oppCluster_0'] * awayBinary

        master['cluster2v1Home'] = master['teamCluster_2'] * master['oppCluster_1'] * homeBinary
        master['cluster2v1Away'] = master['teamCluster_2'] * master['oppCluster_1'] * awayBinary

        master['cluster2v3Home'] = master['teamCluster_2'] * master['oppCluster_3'] * homeBinary
        master['cluster2v3Away'] = master['teamCluster_2'] * master['oppCluster_3'] * awayBinary

        master['cluster3v5Home'] = master['teamCluster_3'] * master['oppCluster_5'] * homeBinary
        master['cluster3v5Away'] = master['teamCluster_3'] * master['oppCluster_5'] * awayBinary

        master['cluster4v2Home'] = master['teamCluster_4'] * master['oppCluster_2'] * homeBinary
        master['cluster4v2Away'] = master['teamCluster_4'] * master['oppCluster_2'] * awayBinary

        master['cluster4v5Home'] = master['teamCluster_4'] * master['oppCluster_5'] * homeBinary
        master['cluster4v5Away'] = master['teamCluster_4'] * master['oppCluster_5'] * awayBinary

        master['cluster5v1Home'] = master['teamCluster_5'] * master['oppCluster_1'] * homeBinary
        master['cluster5v1Away'] = master['teamCluster_5'] * master['oppCluster_1'] * awayBinary

        master['cluster5v3Home'] = master['teamCluster_5'] * master['oppCluster_3'] * homeBinary
        master['cluster5v3Away'] = master['teamCluster_5'] * master['oppCluster_3'] * awayBinary


        if inputDf is None:
            master.to_csv(dataDir + '/processed/trainingData/teamLevelTrainingData.csv', index=False)
        else:
            return master

if __name__ == '__main__':
    app = CreateFeatures()
    app.build_features_team_total()