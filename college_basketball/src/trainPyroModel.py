#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

This trains pyro bayesian inference model

@author: Matt Tetkoskie - 01.04.2022

"""
import os
import pandas as pd
import pyro
import pyro.distributions as dist

# Load data
dataDir = '/Users/mtetkosk/projects/ncaabb_betting_model/college_basketball/data/processed/trainingData'
date = '01-04-2022'
data = pd.read_csv(dataDir + '/%s_trainInput.csv' % date)
print(len(data))

import torch
import numpy as np
from torch import nn
from pyro.nn import PyroModule

assert issubclass(PyroModule[nn.Linear], nn.Linear)
assert issubclass(PyroModule[nn.Linear], PyroModule)

# for CI testing
smoke_test = ('CI' in os.environ)
assert pyro.__version__.startswith('1.8.0')
pyro.set_rng_seed(1)

# Transform into np arrays
data = torch.tensor(data.values, dtype=torch.float)
print(data.shape)

# Define features vs target
x_data, y_data = data[:, :-1], data[:, -1]
print(x_data.shape)
print(x_data.shape[1])

# Define model
linear_reg_model = PyroModule[nn.Linear](x_data.shape[1], 1)

# Define loss and optimize
loss_fn = torch.nn.MSELoss(reduction='sum')
optim = torch.optim.Adam(linear_reg_model.parameters(), lr=0.1)
num_iterations = 3000 if not smoke_test else 2

def train():
    # run the model forward on the data
    y_pred = linear_reg_model(x_data).squeeze(-1)
    # calculate the mse loss
    loss = loss_fn(y_pred, y_data)
    # initialize gradients to zero
    optim.zero_grad()
    # backpropagate
    loss.backward()
    # take a gradient step
    optim.step()
    return loss

for j in range(num_iterations):
    loss = train()
    if (j + 1) % 50 == 0:
        print("[iteration %04d] loss: %.4f" % (j + 1, loss.item()))


print("Learned parameters:")
for name, param in linear_reg_model.named_parameters():
    print(name, param.data.numpy())


from pyro.nn import PyroSample


class BayesianRegression(PyroModule):
    def __init__(self, in_features, out_features):
        super().__init__()
        self.linear = PyroModule[nn.Linear](in_features, out_features)
        self.linear.weight = PyroSample(dist.Normal(0., 1.).expand([out_features, in_features]).to_event(2))
        self.linear.bias = PyroSample(dist.Normal(0., 10.).expand([out_features]).to_event(1))

    def forward(self, x, y=None):
        sigma = pyro.sample("sigma", dist.Uniform(0., 10.))
        mean = self.linear(x).squeeze(-1)
        with pyro.plate("data", x.shape[0]):
            obs = pyro.sample("obs", dist.Normal(mean, sigma), obs=y)
        return mean

from pyro.infer.autoguide import AutoDiagonalNormal

model = BayesianRegression(x_data.shape[1], 1)
guide = AutoDiagonalNormal(model)

from pyro.infer import SVI, Trace_ELBO

adam = pyro.optim.Adam({"lr": 0.03})
svi = SVI(model, guide, adam, loss=Trace_ELBO())

pyro.clear_param_store()
for j in range(num_iterations):
    # calculate the loss and take a gradient step
    loss = svi.step(x_data, y_data)
    if j % 100 == 0:
        print("[iteration %04d] loss: %.4f" % (j + 1, loss / len(data)))

guide.requires_grad_(False)
for name, value in pyro.get_param_store().items():
    print(name, pyro.param(name))


from pyro.infer import Predictive


def summary(samples):
    site_stats = {}
    for k, v in samples.items():
        site_stats[k] = {
            "mean": torch.mean(v, 0),
            "std": torch.std(v, 0),
            "5%": v.kthvalue(int(len(v) * 0.05), dim=0)[0],
            "95%": v.kthvalue(int(len(v) * 0.95), dim=0)[0],
        }
    return site_stats


predictive = Predictive(model, guide=guide, num_samples=800,
                        return_sites=("linear.weight", "obs", "_RETURN"))
samples = predictive(x_data)
pred_summary = summary(samples)

mu = pred_summary["_RETURN"]
y = pred_summary["obs"]
predictions = pd.DataFrame({
    "mu_mean": mu["mean"],
    "mu_perc_5": mu["5%"],
    "mu_perc_95": mu["95%"],
    "y_mean": y["mean"],
    "y_perc_5": y["5%"],
    "y_perc_95": y["95%"],
    "true_score": y_data,
})

print(predictions)
predDir = '/Users/mtetkosk/projects/ncaabb_betting_model/college_basketball/data/predictions'
predictions.to_csv(predDir+'/pyTorchTest.csv', index=False)
