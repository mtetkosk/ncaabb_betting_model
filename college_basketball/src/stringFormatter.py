#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

This contains code for the name formatter class

@author: Matt Tetkoskie - 12.13.2021

"""

import os
import pandas as pd

base_path = os.getcwd().split('college_basketball')[0] + 'college_basketball/'
srcDir = base_path + '/src'


class Format():

    def __init__(self):
        self.nameSheet = pd.read_csv(srcDir + '/nameFormat.csv')
        lookup = {}
        for ix in range(len(self.nameSheet)):
            fromName = self.nameSheet['from_name'][ix]
            toName = self.nameSheet['to_name'][ix]
            lookup[fromName] = toName
        self.nameLookup = lookup

    def format_name(self, name_string):

        try:
            name = name_string.lower()
        except Exception as e:
            return None

        # Replace common characters
        name = name.replace("-", " ")
        name = name.replace("(", "")
        name = name.replace(")", "")
        name = name.replace(".", "")
        name = name.replace("\'", "")
        name = name.replace("state", "st")
        name = name.replace("saint", "st")

        # Map name to/from
        if name in self.nameLookup:
            name = self.nameLookup[name]

        return name.strip()


if __name__ == '__main__':
    app = Format()
    print(app.format_name('IPFW'))
