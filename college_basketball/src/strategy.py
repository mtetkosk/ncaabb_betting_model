#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Strategy Class for NCAA Basketball Prediction

@author: Matt Tetkoskie - 01.25.2022

"""

import os
import pandas as pd
import pickle
import utils
import conf
import ast

class BettingStrategy:

    def __init__(self, inputDF):
        self.version = conf.strategyVersion
        self.models = self.load_models()
        self.inputDF = inputDF
        self.spreadCoverProba = self.load_spread_cover_probas()
        self.winProba = self.load_ml_cover_probas()
        self.winStreaks = self.load_win_streaks()
        self.features = self.create_features()

    def load_spread_cover_probas(self):

        df = pd.read_csv(conf.MODEL_DIR + 'spreadCoverProbas_%s.csv'% self.version)
        # Create Dictionary
        output = {}
        for ix in range(len(df)):
            team = df['team'][ix]
            output[team] = {0: 0.5, 1: 0.5, 2: 0.5}
            val = ast.literal_eval(df['proba'][ix])
            # Parse all/away/home vals
            output[team][2] = val[2]
            output[team][0] = val[0]
            output[team][1] = val[1]
        return output

    def load_win_streaks(self):

        df = pd.read_csv(conf.MODEL_DIR + 'winStreaks_%s.csv'% self.version)
        # Create Dictionary
        output = {}
        for ix in range(len(df)):
            team = df['team'][ix]
            output[team] = {0: 0, 1: 0, 2: 0}
            val = ast.literal_eval(df['streak'][ix])
            # Parse all/away/home vals
            output[team][2] = val[2]
            output[team][0] = val[0]
            output[team][1] = val[1]
        return output

    def load_ml_cover_probas(self):

        df = pd.read_csv(conf.MODEL_DIR + 'winProbas_%s.csv'% self.version)
        # Create Dictionary
        output = {}
        for ix in range(len(df)):
            team = df['team'][ix]
            output[team] = {0: 0.5, 1: 0.5, 2: 0.5}
            val = ast.literal_eval(df['proba'][ix])
            # Parse all/away/home vals
            output[team][2] = val[2]
            output[team][0] = val[0]
            output[team][1] = val[1]
        return output

    def load_models(self):
        models = {}
        for fn in os.listdir(conf.MODEL_DIR):
            if '.pkl' not in fn:
                continue
            if (str(self.version) in fn) and ('_Model' in fn):
                if 'spread' in fn:
                    tag = 'spread'
                else:
                    tag = 'ml'
                print('Loading model %s' % fn)
                model = pickle.load(open(conf.MODEL_DIR + '/' + fn, 'rb'))
                models[tag] = model
        print('Loaded %d model files' % len(models))
        return models

    def map_cover_proba(self, coverType, teamList, homeAwayInd, homeAway=False):
        output = []
        if coverType == 'spread':
            lookup = self.spreadCoverProba
        elif coverType == 'ml':
            lookup = self.winProba

        for ix in range(len(teamList)):
            team = teamList[ix]
            ind = homeAwayInd[ix]
            if team in lookup:
                if not homeAway:
                    output.append(lookup[team][2])
                else:
                    output.append(lookup[team][ind])
            else:
                output.append(0.5)
        return output

    def map_streaks(self,teamList, homeAwayInd, homeAway=False):
        output = []
        for ix in range(len(teamList)):
            team = teamList[ix]
            ind = homeAwayInd[ix]
            if team in self.winStreaks:
                if not homeAway:
                    output.append(self.winStreaks[team][2])
                else:
                    output.append(self.winStreaks[team][ind])
            else:
                output.append(0)
        return output


    def create_features(self):
        inputDF = self.inputDF.copy()
        homeAway = [1 if x =='home' else 0 for x in inputDF['homeAway']]
        inputDF['homeAway'] = homeAway
        inputDF['projectedSpread'] = utils.calculate_spread(inputDF)
        inputDF['projSpreadWin'] = utils.projected_spread_win(inputDF)
        inputDF['projGameWin'] = [1 if x < 0 else 0 for x in inputDF['projectedSpread']]
        inputDF['spread_cover_proba'] = self.map_cover_proba('spread', inputDF['team_name'], inputDF['homeAway'], False)
        inputDF['spread_cover_proba_homeAway'] = self.map_cover_proba('spread', inputDF['team_name'], inputDF['homeAway'], True)
        inputDF['spread_cover_proba_homeAway_diff'] = inputDF['spread_cover_proba'] - inputDF['spread_cover_proba_homeAway']
        inputDF['win_cover_proba'] = self.map_cover_proba('ml', inputDF['team_name'], inputDF['homeAway'], False)
        inputDF['win_cover_proba_homeAway'] = self.map_cover_proba('ml', inputDF['team_name'], inputDF['homeAway'], True)
        inputDF['win_cover_proba_homeAway_diff'] = inputDF['win_cover_proba'] - inputDF['win_cover_proba_homeAway']

        # Streaks
        inputDF['streak_all'] = self.map_streaks(inputDF['team_name'], inputDF['homeAway'], False)
        # Define hot team
        inputDF['streak_all_hot'] = [1 if x >= 4 else 0 for x in inputDF['streak_all']]
        inputDF['streak_all_cold'] = [1 if x <= -4 else 0 for x in inputDF['streak_all']]

        inputDF['streak_homeAway'] = self.map_streaks(inputDF['team_name'], inputDF['homeAway'], True)
        inputDF['streak_homeAway_hot'] = [1 if x >= 4 else 0 for x in inputDF['streak_homeAway']]
        inputDF['streak_homeAway_cold'] = [1 if x <= -4 else 0 for x in inputDF['streak_homeAway']]

        # Define Favorite Underdog 0 = underdog 1 = favorite
        inputDF['vegasFavDog'] = [0 if x < 0 else 1 for x in inputDF['Spread']]
        inputDF['projFavDog'] = [0 if x < 0 else 1 for x in inputDF['projectedSpread']]

        # Create new features
        inputDF['spreadDiff_abs'] = abs(inputDF['projectedSpread'] - inputDF['Spread'])
        inputDF['spreadDiff'] = inputDF['projectedSpread'] - inputDF['Spread']
        inputDF['rankDiff'] = inputDF['Rk'] - inputDF['Rk_opposition']

        # Spread buckets
        inputDF['Underdog3'] = [1 if x <= -25 else 0 for x in inputDF['Spread']]
        inputDF['Underdog2'] = [1 if -25 < x <= -12 else 0 for x in inputDF['Spread']]
        inputDF['Underdog1'] = [1 if -12 < x <= 0 else 0 for x in inputDF['Spread']]
        inputDF['Favorite1'] = [1 if 0 < x <= 12 else 0 for x in inputDF['Spread']]
        inputDF['Favorite2'] = [1 if 12 < x <= 25 else 0 for x in inputDF['Spread']]
        inputDF['Favorite3'] = [1 if x > 25 else 0 for x in inputDF['Spread']]

        # Filter for only values where spread is published
        initialLength = len(inputDF)
        noSpread = inputDF[inputDF['Spread'] != inputDF['Spread']].copy()
        inputDF = inputDF[inputDF['Spread'] == inputDF['Spread']].copy().reset_index(drop=True)
        finalLength = len(inputDF)
        diff = initialLength - finalLength
        if diff > 0:
            print("Removed %d games with no published spread" % diff)
            print(noSpread['team_name'])
        return inputDF

    def get_spread_proba(self):
        """" Calculate Spread Coverage Probabilities """
        # Load features
        orderedFeatures = pd.read_csv(conf.MODEL_DIR + 'spreadStrategy_OrderedFeatures_%s.csv' % self.version)
        X = self.features[orderedFeatures['feature_name']]
        preds = pd.Series(self.models['spread'].predict_proba(X)[:, 1])
        self.features['prediction'] = [round(x, 2) for x in preds]
        # Normalize probabilities
        return utils.transform_proba(self.features)

    def get_ml_proba(self):
        """" Calculate Spread Coverage Probabilities """
        # Load features
        orderedFeatures = pd.read_csv(conf.MODEL_DIR + 'mlStrategy_OrderedFeatures_%s.csv' % self.version)
        X = self.features[orderedFeatures['feature_name']]
        preds = pd.Series(self.models['ml'].predict_proba(X)[:, 1])
        self.features['prediction'] = [round(x, 2) for x in preds]
        # Normalize probabilities
        return utils.transform_proba(self.features)

    def moneyline_bet_indicator(self, df):
        indicator = []
        expectedValue = df['moneyline_expected_value']
        odds = df['MONEYLINE']
        projectedSpread = df['projectedSpread']
        actualSpread = df['Spread']
        for ix in range(len(expectedValue)):
            value = expectedValue[ix]
            proj = projectedSpread[ix]
            vegas = actualSpread[ix]
            if value is None:
                indicator.append(False)
                continue
            betOdds = odds[ix]
            # Favorites
            if value >= conf.mlEVcutoff:
                # Favorites
                if vegas <= 0:
                    if (betOdds >= -300) and (betOdds <= 300) and (-7 < proj < -2):
                        indicator.append(True)
                    else:
                        indicator.append(False)
                else:
                    if (betOdds >= -300) and (betOdds <= 300) and (proj<=vegas):
                        indicator.append(True)
                    else:
                        indicator.append(False)
            else:
                indicator.append(False)
        return indicator

    def spread_bet_indicator(self, df):
        """
        Implement spread pick logic

        # Underdogs projected to win outright with spreadDiff 6>diff>=2.5
        :param df:
        :return:
        """
        indicator = []
        for ix in range(len(df)):
            # Get spread EV > 0:
            if df['spread_expected_value'][ix] < conf.spreadEVcutoff:
                indicator.append(False)
            # Favorites
            elif df['Spread'][ix] <= 0:
                indicator.append(False)
            else:
                indicator.append(True)
        return indicator

    def get_bets(self):
        df = self.features.copy()

        # Get Probabilities
        spreadProba = self.get_spread_proba()
        mlProba = self.get_ml_proba()

        # Get Expected Value
        spreadEV = utils.get_expected_values(df['SpreadOdds'], spreadProba)
        moneylineEV = utils.get_expected_values(df['MONEYLINE'], mlProba)

        # Add to dataframe
        #df['probability_to_cover'] = spreadProba
        df['spread_expected_value'] = spreadEV
        df['probability_to_win'] = mlProba
        df['moneyline_expected_value'] = moneylineEV

        # Flag bet indicators
        df['betIndicator_spread'] = self.spread_bet_indicator(df)
        df['betIndicator_ML'] = self.moneyline_bet_indicator(df)

        return df

if __name__ == "__main__":
    BettingStrategy()

