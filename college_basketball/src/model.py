#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Model class for NCAA Basketball Predictions

@author: Matt Tetkoskie - 01.17.2022

"""

import os
import model_configs
import pandas as pd
import pickle
import utils
import conf
from BuildFeatures import CreateFeatures
from ClusterTeams import ClusterFeatures
clusterEngine = ClusterFeatures()

class Model:

    def __init__(self):
        self.version = conf.modelVersion
        self.models = self.load_models()

    def load_models(self):
        models = {}
        for fn in os.listdir(conf.MODEL_DIR):
            if '.pkl' not in fn:
                continue
            if (str(self.version) in fn) and ('_model' in fn):
                print('Loading model %s' %fn)
                model = pickle.load(open(conf.MODEL_DIR + '/' + fn, 'rb'))
                models[fn] = model
        print('Loaded %d model files' % len(models))
        return models

    def create_features(self, gameData):

        # Load Scaling / Normalizing Files
        orderedFeatures = pd.read_csv(conf.MODEL_DIR + '%s_orderedFeatures.csv' % self.version)
        scaler = pickle.load(open(conf.MODEL_DIR + '%s_scaler.pkl' % self.version, 'rb'))
        pca = pickle.load(open(conf.MODEL_DIR + '%s_pca.pkl' % self.version, 'rb'))

        # Remove Extra Columns
        removeCols = ['boxscore', 'away_abbr', 'home_abbr', 'winning_abbr', 'losing_abbr',
                      'Team_away', 'Team_home', 'Total_away', 'Total_home']
        for col in list(gameData.columns):
            if col in removeCols:
                try:
                    del gameData[col]
                except KeyError:
                    continue

        commonCols = [x for x in list(gameData.columns) if ('_home' in x or '_away' in x)]
        commonColsPrefix = []
        for colName in list(commonCols):
            newCol = colName.replace('_away', '')
            newCol = newCol.replace('_home', '')
            if newCol not in commonColsPrefix:
                commonColsPrefix.append(newCol)

        awayFeatures = utils.collect_features(gameData, commonColsPrefix, 'away')
        homeFeatures = utils.collect_features(gameData, commonColsPrefix, 'home')
        featuresMaster = awayFeatures + homeFeatures
        gameFeatures = pd.DataFrame.from_records(featuresMaster)
        # Assign as attribute so it can be recalled later
        self.gameFeatures = gameFeatures

        gameFeatures.to_csv('test.csv', index=False)
        # Create ML Features
        mlFeatures = CreateFeatures().build_features_team_total(gameFeatures)

        # Assign as attribute so this can be recalled later
        self.mlFeatures = mlFeatures
        # Get ordered list of ml features
        featuresList = [x for x in orderedFeatures['features']]

        # Check to see what if some features are not present
        for featureName in featuresList:
            if featureName not in list(mlFeatures.columns):
                if 'conference' in featureName:
                    mlFeatures[featureName] = 0
                else:
                    raise ValueError('Missing feature %s' % featureName)

        modelInput = mlFeatures[featuresList].copy()
        scaled = pd.DataFrame(scaler.transform(modelInput))
        reduced = pd.DataFrame(pca.transform(scaled))
        return reduced

    def predict(self, inputFeatures):
        """
        :param inputFeatures: pandas dataframe of ML features
        :return: array of score predictions
        """
        predictions = {}
        for model in self.models:
            preds = self.models[model].predict(inputFeatures)
            predictions[model] = preds
            numPreds = len(preds)

        finalPred = []
        for ix in range(numPreds):
            sumVal = 0
            for modelType in predictions:
                sumVal += predictions[modelType][ix]
            finalPred.append(int(sumVal/float(len(predictions))))

        self.mlFeatures['finalPred'] = finalPred

        predicted = self.mlFeatures[['team_name', 'finalPred']].copy()
        output = pd.merge(self.gameFeatures, predicted, on='team_name')
        return output[['team_name', 'gameID', 'finalPred']]

if __name__ == '__main__':
    model = Model()
    features = model.create_features(pd.read_csv('test.csv'))
    preds = model.predict(features)
    print(preds)
