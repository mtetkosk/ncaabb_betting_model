#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Utility function file

@author: Matt Tetkoskie - 12.07.2021

"""
import numpy as np
from stringFormatter import Format
app = Format()

def formatTeamName(nameCol):

    return [app.format_name(x) for x in nameCol]

def formatColumnNames(columns):
    newCols = []
    for col in columns:
        newCol = col.replace('_x', '_away')
        newCol = newCol.replace('_y', '_home')
        newCol = newCol.replace('.1', '_rank')
        newCols.append(newCol)

    return newCols

def collect_features(df, commonCols, homeAway):
    master = []
    for ix in range(len(df)):
        features = {}
        features['team_name'] = df['%s_name' % homeAway][ix]
        features['score'] = df['%s_score' % homeAway][ix]
        features['date'] = df['date'][ix]
        features['rank'] = df['%s_rank' % homeAway][ix]
        features['homeAway'] = homeAway
        features['top25'] = df['top_25'][ix]
        features['gameID'] = df['gameID'][ix]
        # CommonFeatures
        for featureName in commonCols:
            if 'Odds' in featureName:
                continue
            if homeAway == 'away':
                features[featureName] = df[featureName + "_away"][ix]
                if featureName == 'Spread':
                    continue
                features[featureName + '_opposition'] = df[featureName + "_home"][ix]
            if homeAway == 'home':
                features[featureName] = df[featureName + "_home"][ix]
                if featureName == 'Spread':
                    continue
                features[featureName + '_opposition'] = df[featureName + "_away"][ix]
        master.append(features)

    return master

def win_percentage(winLossCol):
    winPct = []
    for record in winLossCol:
        recordSplit = record.split('-')
        pct = round(int(recordSplit[0]) / float(int(recordSplit[0]) + int(recordSplit[1])), 2)
        winPct.append(pct)
    return winPct


def clean_tr_data_col(removal_object, col):

    numericCol = [float(x) for x in col if x != '--']
    colFilter = [x if x != removal_object else np.mean(numericCol) for x in col]
    return colFilter

def calculate_spread(df):
    projectedSpread = []
    for ix in range(len(df)):
        if ix == 0 or ix % 2 == 0:
            diff = df['finalPred'][ix] - df['finalPred'][ix+1]
            projectedSpread.append(diff*-1)
            continue
        if ix % 2 == 1:
            diff = df['finalPred'][ix] - df['finalPred'][ix-1]
            projectedSpread.append(diff*-1)
            continue
    return projectedSpread


def projected_spread_win(df):
    # Projected spread winner
    projSpreadWin = []
    for ix in range(len(df)):
        spread = df['Spread'][ix]
        projected = df['projectedSpread'][ix]
        if spread > 0:
            if spread > projected:
                projSpreadWin.append(1)
            else:
                projSpreadWin.append(0)
        elif spread < 0:
            if projected > spread:
                projSpreadWin.append(0)
            else:
                projSpreadWin.append(1)
        else:
            projSpreadWin.append(0)
    return projSpreadWin

def normalize_probs(x):
    return [round(y/float(sum(x)), 2) for y in x]


def transform_proba(df):

    softMaxProba = []
    processed = []
    for game in df['gameID']:
        if game in processed:
            continue
        sub = df[df['gameID'] == game].copy()
        assert (len(sub) == 2)

        rawProbs = list((sub['prediction']))
        softProbs = normalize_probs(rawProbs)
        softMaxProba.extend(softProbs)
        processed.append(game)

    return softMaxProba

def odds_to_decimal(odds):
    if odds < 0:
        return round(abs(float(100)/odds), 2)
    else:
        return round(abs(odds/float(100)), 2)

def expected_value_bet(proba, odds):
    try:
        eV = round(proba*odds_to_decimal(odds) + -1*(1-proba), 2)
    except Exception:
        print('Proba: %s' % proba)
        print('Odds: %s' % odds)
        eV = None
    return eV

def get_expected_values(odds, proba):
    evList = []
    for ix in range(len(odds)):
        try:
            bet_odds = int(odds[ix])
        except ValueError as e:
            print("Cannot calculate EV: %s" %e)
            evList.append(-1)
            continue
        bet_proba = proba[ix]
        evList.append(expected_value_bet(bet_proba, bet_odds))

    return evList