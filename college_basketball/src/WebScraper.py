#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

This contains code for the webscraper class

@author: Matt Tetkoskie - 12.06.2021

"""

import conf
import pandas as pd
from bs4 import BeautifulSoup
import requests
from datetime import datetime
from sportsipy.ncaab.boxscore import Boxscores


class WebScraper:

    def __init__(self):
        self.date = datetime.today().strftime('%m-%d-%Y')


    def scrape_kenpom(self, writeData=True):

        page = requests.get('https://kenpom.com/')
        if page.status_code != 200:
            raise ConnectionError('Could not load KenPom Data! Status Code: %s' %page.status_code)
        else:
            print('Successfully scraped KenPom Data')
        soup = BeautifulSoup(page.content, 'html.parser')

        tbl = soup.find("table", {"id": "ratings-table"})

        df = pd.read_html(str(tbl))[0]
        # Clean up the header column
        cleansed = [x[-1] for x in df.columns]
        df.columns = cleansed
        keepRows = []
        for ix in range(len(df)):
            val = df['Rk'][ix]
            if val == 'Rk':
                continue
            if type(val) is float:
                continue
            keepRows.append(ix)

        df = df.iloc[keepRows, :].copy().reset_index(drop=True)

        # Write KenPom to csv
        if writeData:
            df.to_csv(conf.DATA_DIR + 'kenPomDaily/' + 'kp_%s.csv' %self.date, index=False)
        newCols = []
        for col in list(df.columns):
            if (col in newCols) and ('.1' not in col):
                col = col+'.1'
            newCols.append(col)
        df.columns = newCols
        return df

    def scrape_games_today(self, writeData=True):

        # Get Games
        games = Boxscores(datetime.today())
        allGames = games._boxscores
        returnDate = list(allGames.keys())[0]
        gamesToday = allGames[returnDate]
        print('Scraped %d games today' %len(gamesToday))

        master = pd.DataFrame()
        for game in gamesToday:
            # Convert scaler to list
            for keyVal in game:
                game[keyVal] = [game[keyVal]]
            sub = pd.DataFrame.from_dict(game)
            master = pd.concat([master,sub])

        if writeData:
            master.to_csv(conf.DATA_DIR + 'games/' + 'games_%s.csv' %self.date, index=False)
        return master

    def scrape_results(self, input_date):

        # Get Games
        scrapeDate = datetime.strptime(input_date, '%m-%d-%Y')
        games = Boxscores(scrapeDate)
        allGames = games._boxscores
        returnDate = list(allGames.keys())[0]
        gamesToday = allGames[returnDate]
        print('Scraped results from %d games' %len(gamesToday))

        master = pd.DataFrame()
        for game in gamesToday:
            # Convert scaler to list
            for keyVal in game:
                game[keyVal] = [game[keyVal]]
            sub = pd.DataFrame.from_dict(game)
            master = pd.concat([master,sub])

        master.to_csv(conf.DATA_DIR + 'results/' + 'game_results_%s.csv' %input_date, index=False)

    @staticmethod
    def cleanTeams(teamCol):
        cleaned = []
        for x in teamCol:
            if 'AM' in x:
                splitVal = 'AM'
            if 'PM' in x:
                splitVal = 'PM'
            if 'Half' in x:
                splitVal = 'Half'

            splits = x.split(splitVal)
            team = splits[1]
            cleaned.append(team)
        return cleaned

    @staticmethod
    def cleanSpread(spreadCol):
        cleaned = []
        for x in spreadCol:
            if x == x:
                try:
                    odds = x[-4:]
                except Exception as e:
                    print(e)
                    print(x)
                    raise ValueError
                splits = x.split(odds)
                spread = splits[0]
                spread = ''.join([x for x in spread if x != '+'])
            else:
                spread = None
            if spread == 'pk':
                spread = 0
            if spread is not None:
                spread = float(spread)
            cleaned.append(spread)
        return cleaned

    @staticmethod
    def spreadOdds(spreadCol):
        cleaned = []
        for x in spreadCol:
            if x != x:
                x = None
            else:
                odds = x[-4:]
            cleaned.append(odds)
        return cleaned

    @staticmethod
    def cleanTotal(totalCol):
        cleaned = []
        for x in totalCol:
            try:
                odds = x[-4:]
            except Exception as e:
                print('Error cleaning total: %s' %e)
                cleaned.append(None)
                continue
            splits = x.split(odds)
            ou = splits[0]
            cleaned.append(ou)
        return cleaned

    @staticmethod
    def TotalOdds(totalCol):
        cleaned = []
        for x in totalCol:
            try:
                odds = x[-4:]
            except Exception as e:
                print('Error cleaning total: %s' %e)
                cleaned.append(None)
                continue
            cleaned.append(odds)
        return cleaned

    def scrape_odds(self, writeData=True):
        url = 'https://sportsbook.draftkings.com/leagues/basketball/88670771'
        page = requests.get(url)
        if page.status_code != 200:
            raise ConnectionError('Could not load DraftKings Data! Status Code: %s' %page.status_code)

        soup = BeautifulSoup(page.content, 'html.parser')
        tbl = soup.find_all("table", {"class": "sportsbook-table"})

        # Build dataframe
        master = pd.DataFrame()
        for x in tbl:
            sub = pd.read_html(str(x))[0]
            print(sub.columns)
            # Manipulate columns
            sub.columns = ['Today', 'SPREAD', 'TOTAL', 'MONEYLINE']
            master = pd.concat([master, sub])

        # Clean columns
        master['Team'] = self.cleanTeams(master['Today'])
        master['Spread'] = self.cleanSpread(master['SPREAD'])
        master['SpreadOdds'] = self.spreadOdds(master['SPREAD'])
        master['Total'] = self.cleanTotal(master['TOTAL'])
        master['TotalOdds'] = self.TotalOdds(master['TOTAL'])

        # Define columns to Keep
        cols = ['Team', 'Spread', 'SpreadOdds', 'Total', 'TotalOdds', 'MONEYLINE']

        if writeData:
            master[cols].to_csv(conf.DATA_DIR + 'OddsDaily/' + 'odds_%s.csv' % self.date, index=False)
        return master[cols]


    def scrape_teamRankings(self, input_date, writeData=True):

        # Scrape points per game
        ppgURL = 'https://www.teamrankings.com/ncaa-basketball/stat/points-per-game'
        params = {'date': input_date}
        r = requests.get(ppgURL, params=params)
        if r.status_code == 200:
            print('Successfully scraped points-per-game data')
        soup = BeautifulSoup(r.content, 'html.parser')
        tbl = soup.find_all("table", {"class": "tr-table datatable scrollable"})
        master = pd.DataFrame()
        for x in tbl:
            sub = pd.read_html(str(x))[0]
            # Manipulate columns
            sub.columns = ['Rank', 'Team', '2021', 'Last 3', 'Last 1', 'Home', 'Away', '2020']
            master = pd.concat([master, sub])

        master.columns = ['Rank', 'Team', '2021_ppg', 'Last3_ppg', 'Last1_ppg', 'Home_ppg', 'Away_ppg', '2020_ppg']
        keepCols = ['Team', '2021_ppg', 'Last3_ppg', 'Last1_ppg', 'Home_ppg', 'Away_ppg']
        master = master[keepCols].copy()


        # Scrape 3 pointer data
        tptURL = "https://www.teamrankings.com/ncaa-basketball/stat/percent-of-points-from-3-pointers"
        r = requests.get(tptURL, params=params)
        if r.status_code == 200:
            print('Successfully scraped 3pt percentage data')
        soup = BeautifulSoup(r.content, 'html.parser')
        tbl = soup.find_all("table", {"class": "tr-table datatable scrollable"})
        tpt_master = pd.DataFrame()
        for x in tbl:
            sub = pd.read_html(str(x))[0]
            # Manipulate columns
            sub.columns = ['Rank', 'Team', '2021', 'Last 3', 'Last 1', 'Home', 'Away', '2020']
            tpt_master = pd.concat([tpt_master, sub])

        tpt_master.columns = ['Rank', 'Team', '2021_3ptpct', 'Last3_3ptpct', 'Last1_3ptpct', 'Home_3ptpct', 'Away_3ptpct', '2020_3ptpct']
        keepCols = ['Team', '2021_3ptpct', 'Last3_3ptpct', 'Last1_3ptpct', 'Home_3ptpct', 'Away_3ptpct']
        tpt_master = tpt_master[keepCols].copy()
        if len(tpt_master) == 0:
            print('No Team Data Scraped!')
            return

        for col in list(tpt_master.columns):
            try:
                tpt_master[col] = [x.replace('%', '') for x in tpt_master[col]]
            except AttributeError:
                continue

        allTR = pd.merge(master, tpt_master, on='Team')

        assert(len(allTR) == len(master))
        assert(len(allTR) == len(tpt_master))

        labelDate = datetime.strptime(input_date, '%Y-%m-%d')
        labelDate = labelDate.strftime('%m-%d-%Y')

        if writeData:
            allTR.to_csv(conf.DATA_DIR + 'teamRankings/' + 'trData_%s.csv' % labelDate, index=False)
        return allTR

if __name__ == '__main__':

    a = WebScraper()
    #a.scrape_kenpom()
    #a.scrape_games_today()
    #a.scrape_odds()
    #a.scrape_results('12-24-2021')
    a.scrape_teamRankings('2022-01-18', writeData=True)
