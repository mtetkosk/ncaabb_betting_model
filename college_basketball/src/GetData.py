#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

This contains code to gather data for basketball predictions

@author: Matt Tetkoskie - 12.07.2021

"""

import os
import pandas as pd
import numpy as np
from datetime import datetime
from datetime import timedelta
from WebScraper import WebScraper
import utils

base_path = os.getcwd().split('college_basketball')[0]  + 'college_basketball/'
dataDir = base_path + '/data/'

class Main():

    def __init__(self, date):

        self.today = date
        print('Today: %s' % self.today)
        self.yesterday = datetime.strptime(self.today, '%m-%d-%Y') - timedelta(days=1)
        self.yesterdayString = self.yesterday.strftime('%m-%d-%Y')
        print('Yesterday: %s' % self.yesterdayString)


        self.scraper = WebScraper()


    def gather_data(self, refresh=True):

        if refresh:
            # Gather data for today and write to file
            print("Start Data Collection")
            # Get kenpom data
            self.scraper.scrape_kenpom()
            # Get game data
            self.scraper.scrape_games_today()
            # Get odds data
            self.scraper.scrape_odds()
            # Get results from yesterday
            self.scraper.scrape_results(self.yesterday.strftime('%m-%d-%Y'))
            print('Data Collection Complete')
            # Get teamrankings data
            self.scraper.scrape_teamRankings(self.yesterday.strftime('%Y-%m-%d'))
        else:
            print('Skipping data collction - Loading previously loaded data')

        # Load Results
        results = pd.read_csv(dataDir + "results/game_results_%s.csv" % self.yesterdayString)
        odds = pd.read_csv(dataDir + 'OddsDaily/odds_%s.csv' % self.yesterdayString)
        stats = pd.read_csv(dataDir + 'kenPomDaily/kp_%s.csv' % self.yesterdayString)
        teamRankings = pd.read_csv(dataDir + 'teamRankings/trData_%s.csv' % self.yesterdayString)

        # Calculate Actual Spread
        results['spread_actual'] = abs(results['home_score'] - results['away_score'])
        results['total_actual'] = results['home_score'] + results['away_score']

        # Format Team Names to facilitate joins
        teamRankings['Team'] = utils.formatTeamName(teamRankings['Team'])
        stats['Team'] = utils.formatTeamName(stats['Team'])
        odds['Team'] = utils.formatTeamName(odds['Team'])
        results['away_name'] = utils.formatTeamName(results['away_name'])
        results['home_name'] = utils.formatTeamName(results['home_name'])
        results['winning_name'] = utils.formatTeamName(results['winning_name'])
        results['losing_name'] = utils.formatTeamName(results['losing_name'])

        # Merge Results and Odds
        # - Do not merge the results and odds - save this for a later step
        # Merge stats with TeamRankings
        statsRankings = pd.merge(stats, teamRankings, on='Team')
        statsMerge = pd.merge(results, statsRankings, left_on='away_name', right_on='Team')
        statsMerge = pd.merge(statsMerge, statsRankings, left_on='home_name', right_on='Team')
        statsMerge.columns = utils.formatColumnNames(list(statsMerge.columns))

        for x in results['away_name']:
            if x not in list(statsMerge['away_name']):
                print('Could not find matches for away team: %s' % x)

        for x in results['home_name']:
            if x not in list(statsMerge['home_name']):
                print('Could not find matches for home team: %s' % x)

        # Add date string
        statsMerge['date'] = self.yesterdayString

        # Clean Total Column
        # Create unique gameID
        statsMerge['gameID'] = statsMerge['date'] + '-' + statsMerge['away_abbr'] + '-' + statsMerge['home_abbr']

        removeCols = ['boxscore', 'away_abbr', 'home_abbr', 'winning_abbr', 'losing_abbr',
                      'Team_away', 'Team_home', 'Total_away', 'Total_home']

        for col in list(statsMerge.columns):
            if col in removeCols:
                try:
                    del statsMerge[col]
                except KeyError:
                    continue

        # Write game level data to file
        statsMerge.to_csv(dataDir + 'processed/gameData/gameData_%s.csv'% self.yesterdayString, index=False)

        # Parse out team level data
        commonCols = [x for x in list(statsMerge.columns) if ('_home' in x or '_away' in x)]
        commonColsPrefix = []
        for colName in list(commonCols):
            newCol = colName.replace('_away', '')
            newCol = newCol.replace('_home', '')
            if newCol not in commonColsPrefix:
                commonColsPrefix.append(newCol)

        awayFeatures = utils.collect_features(statsMerge, commonColsPrefix, 'away')
        homeFeatures = utils.collect_features(statsMerge, commonColsPrefix, 'home')
        featuresMaster = awayFeatures + homeFeatures
        gameFeatures = pd.DataFrame.from_records(featuresMaster)

        gameFeatures.to_csv(dataDir + 'processed/teamData/teamData_%s.csv'% self.yesterdayString, index=False)

if __name__ == '__main__':
    dateInput = datetime.today().strftime('%m-%d-%Y')
    #dateInput = '01-26-2022'
    app = Main(dateInput)
    app.gather_data(refresh=True)
