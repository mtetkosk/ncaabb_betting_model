#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Clustering Class for NCAA Basketball Predictions

@author: Matt Tetkoskie - 01.20.2022

"""
import conf
import numpy as np
import pandas as pd
import pickle

class ClusterFeatures:

    def __init__(self):
        self.columns = pd.read_csv(conf.MODEL_DIR+'clusterCols.csv')
        self.scaler = pickle.load(open(conf.MODEL_DIR+'KPscaler.pkl','rb'))
        self.model = pickle.load(open(conf.MODEL_DIR+'KMeans.pkl','rb'))

        self.masterData = pd.read_csv(conf.DATA_DIR+'/processed/trainingData/teamLevelTrainingData.csv')

    def clusterTeam(self, teamData):
        # Get relevant columns
        assert(len(teamData.columns) == len(self.columns))
        # Transform
        clusterData = self.scaler.transform(teamData)
        clusterLabel = list(self.model.predict(clusterData))
        return clusterLabel

    def populate_matchup_data(self, scoresDF = None):

        if scoresDF is None:
            scoresDF = self.masterData

        # Cluster Teams
        teamCluster = self.clusterTeam(scoresDF[['AdjEM', 'AdjO', 'AdjD', 'AdjT']].copy())
        scoresDF['teamCluster'] = teamCluster
        oppCluster = self.clusterTeam(scoresDF[['AdjEM_opposition', 'AdjO_opposition', 'AdjD_opposition', 'AdjT_opposition']].copy())
        scoresDF['oppCluster'] = oppCluster

        # Generate Stats For Each Cluster
        clusterMean = {}
        MatchupMean = {}
        clusterRange = np.arange(0, 6, 1)

        for cluster in clusterRange:
            sub = scoresDF[scoresDF['teamCluster'] == cluster].copy()
            if len(sub) > 0:
                clusterMean[cluster] = round(sub['score'].mean(), 1)
            else:
                clusterMean[cluster] = round(scoresDF['score'].mean(), 1)

            MatchupMean[cluster] = {}
            for oppCluster in clusterRange:
                oppSub = sub[sub['oppCluster'] == oppCluster].copy()
                MatchupMean[cluster][oppCluster] = {}
                # Get matchup mean for all games
                if len(oppSub) > 3:
                    MatchupMean[cluster][oppCluster]['all'] = round(oppSub['score'].mean(), 1)
                else:
                    MatchupMean[cluster][oppCluster]['all'] = round(sub['score'].mean(), 1)

                # Get matchup mean for home games
                oppSubHome = oppSub[oppSub['home_away'] == 1].copy()
                if len(oppSubHome) > 3:
                    MatchupMean[cluster][oppCluster]['home'] = round(oppSubHome['score'].mean(), 1)
                else:
                    MatchupMean[cluster][oppCluster]['home'] = round(oppSub['score'].mean(), 1)

                # Get matchup mean for away games
                oppSubAway = oppSub[oppSub['home_away'] == -1].copy()
                if len(oppSubHome) > 3:
                    MatchupMean[cluster][oppCluster]['away'] = round(oppSubAway['score'].mean(), 1)
                else:
                    MatchupMean[cluster][oppCluster]['away'] = round(oppSub['score'].mean(), 1)

        self.clusterMean = clusterMean
        self.MatchupMean = MatchupMean

    def cluster_mean(self, teamCluster):
        return self.clusterMean[teamCluster]

    def matchup_mean(self, teamCluster, oppCluster):
        allMean = self.MatchupMean[teamCluster][oppCluster]['all']
        homeMean = self.MatchupMean[teamCluster][oppCluster]['home']
        if homeMean != homeMean:
            homeMean = allMean
        awayMean = self.MatchupMean[teamCluster][oppCluster]['away']
        if awayMean != awayMean:
            awayMean = allMean

        return {"all": allMean, "home": homeMean, "away": awayMean}


if __name__ == '__main__':
    a = ClusterFeatures()
    test = pd.read_csv(conf.DATA_DIR + 'processed/trainingData/01-19-2022_trainInput_raw.csv')
    a.populate_matchup_data()
    print(a.matchup_mean(5, 9))
